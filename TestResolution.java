public class TestResolution
{
    public static void main (String args[])
    {
        Labyrinthe lab = new Labyrinthe();

        //premier test
        System.out.println("TEST : Résolution du labyrinthe 1 : ");
        lab = new Labyrinthe();
        lab.ajouterEtat("entrée", true, false);
        lab.ajouterEtat("e1", false, false);
        lab.ajouterEtat("e2", false, false);
        lab.ajouterEtat("e3", false, false);
        lab.ajouterEtat("e4", false, false);
        lab.ajouterEtat("sortie", false, true);

        lab.lier("entrée", "e1");
        lab.lier("entrée", "e2");
        lab.lier("entrée", "e3");
        lab.lier("e1", "e3");
        lab.lier("e1", "e4");
        lab.lier("e2", "e4");
        lab.lier("e3", "sortie");
        lab.lier("e4", "sortie");

        System.out.println(lab.resoudre());

        //second test
        System.out.println("TEST : Résolution du labyrinthe 2 : (1er essai) ");
        lab = new Labyrinthe();
        lab.ajouterEtat("entrée", true, false);
        lab.ajouterEtat("e1", false, false);
        lab.ajouterEtat("e2", false, false);
        lab.ajouterEtat("e3", false, false);
        lab.ajouterEtat("e4", false, false);
        lab.ajouterEtat("e5", false, false);
        lab.ajouterEtat("e6", false, false);
        lab.ajouterEtat("e7", false, false);
        lab.ajouterEtat("e8", false, false);
        lab.ajouterEtat("e9", false, false);
        lab.ajouterEtat("sortie", false, true);

        lab.lier("entrée", "e1");

        lab.lier("e1", "e2");
        lab.lier("e1", "e7"); //echanger ces deux lignes permet d'obtenir un chemin différent

        lab.lier("e2", "e3");
        lab.lier("e2", "e8");
        lab.lier("e3", "e4");
        lab.lier("e3", "e8");
        lab.lier("e4", "e5");
        lab.lier("e4", "e6");
        lab.lier("e4", "e9");
        lab.lier("e5", "e6");
        lab.lier("e6", "sortie");
        lab.lier("e7", "e8");
        lab.lier("e8", "e9");
        lab.lier("e9", "sortie");

        System.out.println(lab.resoudre());

        System.out.println("TEST : Résolution du labyrinthe 2 : (2nd essai) ");
        lab = new Labyrinthe();
        lab.ajouterEtat("entrée", true, false);
        lab.ajouterEtat("e1", false, false);
        lab.ajouterEtat("e2", false, false);
        lab.ajouterEtat("e3", false, false);
        lab.ajouterEtat("e4", false, false);
        lab.ajouterEtat("e5", false, false);
        lab.ajouterEtat("e6", false, false);
        lab.ajouterEtat("e7", false, false);
        lab.ajouterEtat("e8", false, false);
        lab.ajouterEtat("e9", false, false);
        lab.ajouterEtat("sortie", false, true);

        lab.lier("entrée", "e1");

        lab.lier("e1", "e7");
        lab.lier("e1", "e2");//echanger ces deux lignes permet d'obtenir un chemin différent

        lab.lier("e2", "e3");
        lab.lier("e2", "e8");
        lab.lier("e3", "e4");
        lab.lier("e3", "e8");
        lab.lier("e4", "e5");
        lab.lier("e4", "e6");
        lab.lier("e4", "e9");
        lab.lier("e5", "e6");
        lab.lier("e6", "sortie");
        lab.lier("e7", "e8");
        lab.lier("e8", "e9");
        lab.lier("e9", "sortie");

        System.out.println(lab.resoudre());





        //troisième test :
        System.out.println("TEST : Résolution du labyrinthe 3 : ");
        lab = new Labyrinthe();
        lab.ajouterEtat("entrée", true, false);
        lab.ajouterEtat("e1", false, false);
        lab.ajouterEtat("e2", false, false);
        lab.ajouterEtat("e3", false, false);
        lab.ajouterEtat("e4", false, false);
        lab.ajouterEtat("e5", false, false);
        lab.ajouterEtat("e6", false, false);
        lab.ajouterEtat("e7", false, false);
        lab.ajouterEtat("e8", false, false);
        lab.ajouterEtat("e9", false, false);
        lab.ajouterEtat("e10", false, false);
        lab.ajouterEtat("sortie", false, true);

        lab.lier("entrée", "e1");
        lab.lier("entrée", "e2");

        lab.lier("e1", "e3");
        lab.lier("e1", "e5");

        lab.lier("e2", "e4");

        lab.lier("e3", "e10");
        lab.lier("e3", "e7");

        lab.lier("e4", "e5");
        lab.lier("e4", "e9");

        lab.lier("e5", "e6");
        lab.lier("e5", "e8");

        lab.lier("e6", "e7");
        lab.lier("e6", "e8");
        lab.lier("e6", "sortie");

        lab.lier("e7", "sortie");

        lab.lier("e8", "sortie");

        lab.lier("e9", "sortie");

        System.out.println(lab.resoudre());


    }
}
