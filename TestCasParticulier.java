public class TestCasParticulier
{
    public static void main (String args[])
    {
        Labyrinthe lab = new Labyrinthe();

        System.out.println("TEST : Résolution du labyrinthe sans entrée : ");

        lab.ajouterEtat("e1", false, false);
        lab.ajouterEtat("sortie", false, true);
        System.out.println(lab.resoudre());

        System.out.println("TEST : Résolution du labyrinthe sans solution : ");
        lab.ajouterEtat("entrée", true, false);
        lab.lier("entrée", "e1");
        System.out.println(lab.resoudre());

        System.out.println("TEST : Résolution du labyrinthe avec plusieurs entrées : ");
        lab.ajouterEtat("entrée2", true, false);
        lab.lier("e1","sortie") ;
        System.out.println(lab.resoudre());

        lab = new Labyrinthe();

        System.out.println("TEST : Résolution du labyrinthe sans entrée ni sortie : ");
        lab.ajouterEtat("e1", false, false);
        lab.ajouterEtat("e2", false, false);
        lab.lier("e1", "e2");
        System.out.println(lab.resoudre());


        System.out.println("TEST : Résolution du labyrinthe sans sortie : ");
        lab.ajouterEtat("entrée", true, false);
        lab.lier("entrée", "e1");
        System.out.println(lab.resoudre());


        System.out.println("TEST : Résolution du labyrinthe avec plusieurs sorties : ");
        lab.ajouterEtat("sortie",false, true);
        lab.ajouterEtat("sortie2",false, true);
        lab.lier("e1","sortie") ;
        System.out.println(lab.resoudre());

        System.out.println("TEST : Résolution du labyrinthe avec plusieurs sorties et plusieurs entrées : ");
        lab.ajouterEtat("entrée2",true, false);
        lab.lier("e1","sortie") ;
        System.out.println(lab.resoudre());






    }
}
