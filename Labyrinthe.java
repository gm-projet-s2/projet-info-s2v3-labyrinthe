
import java.util.ArrayList;
import java.util.LinkedList ;

public class Labyrinthe {

    private ArrayList<Etat> etats;

    // Constructeurs

    public Labyrinthe(){
        this.etats = new ArrayList<Etat>();

    }

    public Labyrinthe(ArrayList<Etat> etats){
        this.etats = etats;
    }

    // Accesseur et mutateur

    public ArrayList<Etat> getEtats(){
        return this.etats;
    }

    public void setEtats(ArrayList<Etat> etats){
        this.etats = etats;
    }

    public void ajouterEtat(String nom, boolean estEntree, boolean estSortie){
        this.etats.add(new Etat(nom, new ArrayList<Etat>(), estEntree, estSortie));
    }

    public void supprimerEtat(Etat etat){
        // on parcourt tous les adjacents de etat et on les dissocie de lui
        for (Etat adj : etat.getAdjacents())
            adj.getAdjacents().remove(etat);
        // puis on retire etat du labyrinthe
        this.getEtats().remove(etat);
        etat = null;
    }

    public Labyrinthe clone()   //renvoi une copie du Labyrinthe pour pouvoir le manipuler sans modifier l'original
    {
        ArrayList<Etat> etatsClone = new ArrayList<Etat>(this.getEtats());  //permet de creer un clone de la liste d'états
        return new Labyrinthe(etatsClone) ;     //on renvoi un nouveau labyrinthe créer à partir du clone de la liste
    }

    public Etat entree() throws NombreEntreeSortieException{
        // lever exceptions : pas d'entrée, plusieurs entrées
        Etat etatEntree = null;
        int nbEntree = 0;
        // On parcourt les états et on compte les nombre d'entrées du labyrinthe
        for (Etat etat : this.getEtats()){
            if (etat.getEstEntree()){
                etatEntree = etat;
                nbEntree++;
            }
        }
        if (nbEntree != 1){
            etatEntree = null;
            throw new NombreEntreeSortieException(true, nbEntree);
        }
        return etatEntree;
    }

    public Etat sortie() throws NombreEntreeSortieException{
        // lever exceptions : pas d'entrée, plusieurs sorties
        Etat etatSortie = null;
        int nbSortie = 0;
        // On parcourt les états et on compte les nombre de sorties du labyrinthe
        for (Etat etat : this.getEtats()){
            if (etat.getEstSortie()){
                etatSortie = etat;
                nbSortie++;
            }
        }
        if (nbSortie != 1){
            etatSortie = null;
            throw new NombreEntreeSortieException(false, nbSortie);
        }
        return etatSortie;
    }

    public boolean estValide(){
        /* Un labyrinthe valide est un labyrinthe avec une seule entrée et une seule sortie.
            Ce n'est pas forcément un labyrinthe irrésolvable, c'est un labyrinthe que le solveur
            saura prendre en compte.
        */
        boolean valide = true;
        try {
            valide = (this.entree() != null) && valide;;
        } catch(NombreEntreeSortieException e) {
            valide = false ;
            e.printStackTrace();
        }

        try {
            valide = (this.sortie() != null) && valide;
        } catch(NombreEntreeSortieException e) {
            valide = false;
            e.printStackTrace();
        }

        return valide;
    }

    public boolean estVide(){
        return etats.isEmpty();
    }


    public void lier(String nom1, String nom2){
        Etat etat1 = null, etat2 = null;
        // on recherche les deux états dont le nom est en paramètre
        for (Etat e : this.etats){
            if (e.getNom() == nom1)
                etat1 = e;
            if (e.getNom() == nom2)
                etat2 = e;
        }
        // on tente de les connecter
        try{
            etat1.ajouterAdjacent(etat2);
        }catch(NullPointerException e){
            System.out.println("Un de ces états n'existe pas !");
        }
    }

    public LinkedList<Etat> resoudre() //permet d'obtenir un chemin allant de l'entrée à la sortie
    {
        LinkedList<Etat> res = new LinkedList<Etat>() ;

        if (this.estValide())
        {
            Solveur solv = new Solveur(this) ;
            res = solv.trouverSol() ;
        }
        if (res.isEmpty()) //si le solveur n'a pas trouvé de solution ou si le labyrinthe n'est pas valide on renvoi l'Etat echec
        {
            res.add(new Etat("Echec",new ArrayList<Etat>(),false,false));
        }
        return res ;
    }
}
