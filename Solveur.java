import java.util.ArrayList ;
import java.util.LinkedList ;
import java.util.Iterator ;

public class Solveur
{
    //attributs
    private Labyrinthe labyrinthe ;

    //Constructeur
    public Solveur(Labyrinthe laby)
    {
        this.labyrinthe = laby ;
    }

    //accesseurs
    public void setLabyrinthe(Labyrinthe laby)
    {
        this.labyrinthe = laby ;
    }
    public Labyrinthe getLabyrinthe()
    {
        return this.labyrinthe ;
    }

    //methodes
    public LinkedList<Etat> trouverSol()
    {
        LinkedList<Etat> res = new LinkedList<Etat>() ; //le résultat par défaut est une liste vide

        try
        {
            if (!this.getLabyrinthe().estVide())    //si le labyrinthe n'est pas vide
            {
                Etat debut = this.getLabyrinthe().entree() ;    //on récupère l'etat de départ
                if (debut.getEstSortie())
                {
                    res.add(debut) ;    //si l'état de départ est l'état de fin on renvoi une liste contenant seulement cet etat
                }
                else
                {
                    Iterator<Etat> ite = debut.getAdjacents().iterator();
                    Solveur solv = new Solveur(this.getLabyrinthe().clone());   //on crée un nouveau Solveur avec un clone du Labyrinthe 
                    solv.getLabyrinthe().supprimerEtat(debut);      //on enlève le point de départ

                    //on parcourt les adjacents de l'etats de depart jusqu'à ce qu'on trouve un chemin ou jusqu'à ce qu'on les ait tous parcouru
                    while (ite.hasNext() && res.isEmpty())
                    {
                        Etat adjacent = ite.next(); //on  récupère le prochain adjacent
                        adjacent.setEstEntree(true);    //on le fixe comme point de départ
                        LinkedList<Etat> temp = solv.trouverSol();  //on cherche une solution dans le nouveau Labyrinthe
                        if (!temp.isEmpty())    // si on trouve une solution
                        {
                            res.add(debut); // la solution commence par le point de départ du labyrinthe
                            res.addAll(temp);   //la suite de la solution est la solution en partant de l'adjacent
                        }
                        adjacent.setEstEntree(false);   // pour permettre de changer de début à la prochaine boucle
                    }
                }
            }
        }
        catch (NombreEntreeSortieException e)
        {
            e.printStackTrace();
        }
        return res ;
    }

}
