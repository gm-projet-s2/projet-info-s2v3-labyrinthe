
import java.util.ArrayList;

public class Etat {

    private String nom;
    private ArrayList<Etat> adjacents;
    private boolean estEntree;
    private boolean estSortie;

    // Constructeur

    public Etat(String nom, ArrayList<Etat> adjacents, boolean estEntree, boolean estSortie){
        this.nom = nom;
        this.adjacents = adjacents;
        this.estEntree = estEntree;
        this.estSortie = estSortie;
    }

    // Accesseurs

    public String getNom(){
        return this.nom;
    }

    public ArrayList<Etat> getAdjacents(){
        return this.adjacents;
    }

    public boolean getEstEntree(){
        return this.estEntree;
    }

    public boolean getEstSortie(){
        return this.estSortie;
    }

    // Mutateurs

    public void setNom(String nom){
        this.nom = nom;
    }

    public void setAdjacents(ArrayList<Etat> adjacents){
        this.adjacents = adjacents;
    }

    public void setEstEntree(boolean estEntree){
        this.estEntree = estEntree;
    }

    public void setEstSortie(boolean estSortie){
        this.estSortie = estSortie;
    }

//
    public String toString ()
    {
        return this.getNom();
    }



    public void ajouterAdjacent(Etat etat){
        this.adjacents.add(etat);
        etat.adjacents.add(this);
    }

    public void enleverAdjacent(Etat etat){
        this.adjacents.remove(etat);
        etat.adjacents.remove(this);
    }

    public boolean equals(Etat etat){
        boolean egal = (this.nom == etat.nom);
        egal = (egal && (this.adjacents.equals(etat.adjacents)));
        egal = (egal && (this.estEntree == etat.estEntree));
        egal = (egal && (this.estSortie == etat.estSortie));
        return egal;
    }

}
