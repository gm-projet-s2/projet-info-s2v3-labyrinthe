
public class NombreEntreeSortieException extends Exception {

    public NombreEntreeSortieException(boolean entree, int nombre){
        super();
        System.out.println("Pas le bon nombre d" + (entree ? "'entrée(s) : " : "e sortie(s) : ") + nombre);
    }

}
